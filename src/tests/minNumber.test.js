const minNumber = require('../minNumber')

test('when given 2 numbers', () => {
    const arr = [4, 6, 2, 3]
    expect(minNumber(arr)).toBe(2)
})